import { atom } from "jotai";
import { getCalculatedDate } from "../util/timeTools";

// let miaCycleLength = 2100;

export const interestingBlocks = [
  {
    blockNumber: 34497,
    description: "MIA rewards drop to 100K",
    source:
      "https://docs.citycoins.co/citycoins-core-protocol/issuance-schedule",
  },
  {
    blockNumber: 34497,
    description: "MIA Cycle 4 ends",
    source:
      "https://docs.citycoins.co/citycoins-core-protocol/stacking-citycoins",
  },
  {
    blockNumber: 234497,
    description: "MIA 2nd halving",
    source:
      "https://docs.citycoins.co/citycoins-core-protocol/issuance-schedule",
  },
];

const interestingBlocksAtom = atom(interestingBlocks);

export const interestingBlocksFetch = atom(async (get) => {
  let blockEvents = [];
  const events = get(interestingBlocksAtom);
  for (let i = 0; i < events.length; i++) {
    let date = await await getCalculatedDate(events[i].blockNumber);
    blockEvents.push({
      blockNumber: events[i].blockNumber,
      blockDate: date,
      description: events[i].description,
      source: events[i].source,
    });
  }

  return blockEvents;
});
